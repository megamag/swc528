﻿using NetworkAdapters;
using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Threading;
using System.Threading.Tasks;

namespace ServerService
{
    /// <summary>
    /// Класс-распознаватель новых объектов на изображении
    /// </summary>
    class ImageProcessor : Task<ImageProcessorState>
    {
        /// <summary>
        /// Конструктор класса, создаёт объект класса и запускает его процесс асинхронно
        /// </summary>
        /// <param name="initialState">Объектс с изображения для сравнения</param>
        /// <param name="tokenSource">Токен объекта</param>
        public ImageProcessor(ImageProcessorState initialState, CancellationTokenSource tokenSource) 
            : base(CompareImages, initialState, tokenSource.Token)
        {
            if (initialState.Image1 == null || initialState.Image2 == null)
                throw new ArgumentNullException();
            Start();
        }

        /// <summary>
        /// Функция-делегат сравнения ищображений переданного объекта типа ImageProcessorState
        /// </summary>
        private static Func<object, ImageProcessorState> CompareImages = (State) =>
        {
            var picture1 = new Bitmap(new MemoryStream((State as ImageProcessorState).Image1));
            var picture2 = new Bitmap(new MemoryStream((State as ImageProcessorState).Image1));
            int iMax = picture1.Width;
            int jMax = picture1.Height;
            int totalPixelsCount = iMax * jMax;
            if ((picture2.Width * picture2.Height != totalPixelsCount) || (picture1.PixelFormat != picture2.PixelFormat))
                throw new FormatException();

            var data1 = picture1.LockBits(new Rectangle(0, 0, iMax, jMax), ImageLockMode.ReadOnly, picture1.PixelFormat);
            var data2 = picture2.LockBits(new Rectangle(0, 0, iMax, jMax), ImageLockMode.ReadOnly, picture2.PixelFormat);
            int depth = Bitmap.GetPixelFormatSize(picture1.PixelFormat) / 8;

            var buffer1 = new byte[totalPixelsCount * depth];
            var buffer2 = new byte[totalPixelsCount * depth];
            var bufferDiff = new byte[totalPixelsCount * depth];

            System.Runtime.InteropServices.Marshal.Copy(data1.Scan0, buffer1, 0, buffer1.Length);
            System.Runtime.InteropServices.Marshal.Copy(data2.Scan0, buffer2, 0, buffer2.Length);

            int maxval = 0, minval = 255;
            Parallel.For(0, totalPixelsCount, (k) => 
            {
                for (int c = 0; c < depth; c++)
                {
                    int diff = Math.Abs(buffer1[k + c] - buffer2[k + c]);
                    if (maxval < diff) { Interlocked.Exchange(ref maxval, diff); }
                    if (minval > diff) { Interlocked.Exchange(ref minval, diff); }
                    bufferDiff[k + c] = (byte)diff;
                }
            });

            double medianValue = 0.5 * (maxval - minval);
            int differentPixelsCount = 0;
            int minCol = iMax, maxCol = 0;
            int minRow = jMax, maxRow = 0;
            Parallel.For(0, iMax, (i) =>
            {
                Parallel.For(0, jMax, (j) => 
                {
                    int offset = (j * iMax + i) * depth;
                    for (int c = 0; c < depth; c++)
                    {
                        if (bufferDiff[offset + c] > medianValue)
                        {
                            Interlocked.Add(ref differentPixelsCount, 1);

                            if (i < minCol)
                                Interlocked.Exchange(ref minCol, i);
                            else if (i > maxCol)
                                Interlocked.Exchange(ref maxCol, i);

                            if (j < minRow)
                                Interlocked.Exchange(ref minRow, j);
                            else if (j > maxRow)
                                Interlocked.Exchange(ref maxRow, j);

                            break;
                        }
                    }
                });
            });

            picture1.UnlockBits(data1);
            picture2.UnlockBits(data2);

			var Result = new ImageProcessorState {
				IsDetected = true,
				DifferentPixelsCount = (uint)differentPixelsCount,
				DetectedRectangle = new byte[8] {
					(byte)(minCol / 256), (byte)(minCol % 256),
					(byte)(minRow / 256), (byte)(minRow % 256),
					(byte)(maxCol / 256), (byte)(maxCol % 256),
					(byte)(maxRow / 256), (byte)(maxRow % 256)
				}
				//DetectedRectangle = new Rectangle(minCol, minRow, maxCol - minCol, maxRow - minRow)
			};
			picture1.Save(new MemoryStream(Result.Image1), picture1.RawFormat);
			picture2.Save(new MemoryStream(Result.Image2), picture2.RawFormat);
			return Result;
        };
    }
}
