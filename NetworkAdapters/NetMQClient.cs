﻿using System.Collections.Generic;
using System.Threading;
using NetMQ;
using NetMQ.Sockets;
using Google.Protobuf.COOM;

namespace NetworkAdapters
{
    /// <summary>
	/// Класс-обёртка для сокетов запросов и подписок
	/// </summary>
	/// <list type="bullet">
	/// <item>
	/// <term>ReadyRead</term>
	/// <description>Ивент для запуска обработки принятых сообщений</description>
	/// </item>
	/// <item>
	/// <term>requestSocket</term>
	/// <description>Сокет запросов в системе запрос-ответ</description>
	/// </item>
	/// <item>
	/// <term>subscribeSocket</term>
	/// <description>Сокет подписки в системе подписчик-публикатор</description>
	/// </item>
	/// <item>
	/// <term>subscriberThread</term>
	/// <description>Поток обработки сообщений сокета подписчика</description>
	/// </item>
	/// </list>
	public class NetMQClient
	{
        public event Messaging ReadyRead;

		private readonly RequestSocket requestSocket;
		private readonly SubscriberSocket subscribeSocket;

		private Thread subscriberThread;

        /// <summary>
        /// Метод запуска потока обработки сообщений подписчика (<c>subscriberThread</c>).
        /// </summary>
        public void Start()
		{
			if (subscriberThread == null || !subscriberThread.IsAlive)
			{
				subscribeSocket.Subscribe("");

				subscriberThread = new Thread(() => {
					while (true)
					{
						var data = subscribeSocket.ReceiveMultipartBytes();
						var message = new List<byte>();
						foreach (var part in data)
							message.AddRange(part);
						var imageEvent = ImageEvent.Parser.ParseFrom(message.ToArray());
						ReadyRead?.Invoke(new ImageProcessorState(imageEvent));
					}
				});
				subscriberThread.Start();
			}
		}

		/// <summary>
		/// Метод, автоматически вызываемый для обработки ответов на запросы.
		/// </summary>
		/// <param name="sender">Объект, описывающий отправителя сообщения</param>
        /// <param name="e">Объект, описывающий событие сокета с сообщением</param>
		private void requestSocket_ReadyRead(object sender, NetMQSocketEventArgs e)
		{
			var data = e.Socket.ReceiveMultipartBytes();
			var message = new List<byte>();
			foreach (var part in data)
				message.AddRange(part);
			ReadyRead?.Invoke(ImageEvent.Parser.ParseFrom(message.ToArray()));
		}
        /// <summary>
        /// Метод отправки запроса.
        /// </summary>
        /// <param name="message">Сообщение запроса</param>
		public object Request(object message)
		{
			var request = (message as ImageProcessorState).GetMessage();
			var bytes = new byte[request.CalculateSize()];
			request.WriteTo(new Google.Protobuf.CodedOutputStream(bytes));
			requestSocket.SendMultipartBytes(bytes);
			return null;
		}
        /// <summary>
		/// Конструктор обёртки на основе адресов для сокетов запроса и подписки.
		/// </summary>
        /// <param name="publishAddress">Адрес сокета запроса</param>
        /// <param name="subscribeAddress">Адрес сокета подписки</param>
		public NetMQClient(string publishAddress, string subscribeAddress)
		{
			if (subscribeAddress.Length == 0)
				subscribeAddress = ">tcp://localhost:" + GlobalVariables.PublisherPort;
			if (publishAddress.Length == 0)
				publishAddress = ">tcp://localhost:" + GlobalVariables.ResponderPort;

			subscribeSocket = new SubscriberSocket(subscribeAddress);
			requestSocket = new RequestSocket(publishAddress);
			requestSocket.ReceiveReady += requestSocket_ReadyRead;
		}
		
		/// <summary>
		/// Деструктор
		/// </summary>
		~NetMQClient()
		{
			subscriberThread.Abort();
		}
	}
}
