﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace OperatorPanel
{
	/// <summary>
	/// Класс, содержащий функции рисования средствами WPF ImageBrush.
	/// </summary>
	/// <example>
	/// <code>
	/// DrawingGroup imageToDrawOn = Drawer2D.DrawSetup(initialImage, timestamp);
	/// imageToDrawOn.Children.Add(Drawer2D.DrawPolygon(polygon, Brushes.Red).Drawing);
	/// windowImageViewport.Source = new DrawingImage(imageToDrawOn);
	/// </code>
	/// </example>
	class Drawer2D
	{
		/// <value>
		/// Положение метки времени кадра на изображении.
		/// </value>
		public static Point timestampPosition = new Point(16, 16);

		/// <summary>
		/// Принять битовое изображение и преобразовать его в тип, с которы может работать WPF ImageBrush.
		/// </summary>
		/// <param name="src">Битовое изображение (<c>System.Drawing.Bitmap</c>)</param>
		/// <returns>Совместимое с WPF ImageBrush изображение (<c>System.Windows.Media.Imaging.BitmapImage</c>)</returns>
		private static BitmapImage GetImage(System.Drawing.Bitmap bitmap)
		{
			MemoryStream ms = new MemoryStream();
			bitmap.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
			var image = new BitmapImage();
			image.BeginInit();
			ms.Seek(0, SeekOrigin.Begin);
			image.StreamSource = ms;
			image.EndInit();
			return image;
		}

		/// <summary>
		/// Принять битовое изображение и подготовить его к работе с WPF ImageBrush.
		/// </summary>
		/// <param name="image">Битовое изображение (<c>System.Drawing.Bitmap</c>)</param>
		/// <param name="time">Метка времени кадра</param>
		/// <returns>Контейнер DrawingGroup для отрисовки в WPF</returns>
		public static DrawingGroup DrawSetup(System.Drawing.Bitmap image, DateTime time)
		{
			var Gr = new DrawingGroup();
			
			Gr.Children.Add(
				new ImageDrawing(
					GetImage(image), 
					new Rect(0, 0, image.Width, image.Height)
					));

			var Timestamp = new FormattedText(
					string.Format("{0}", time),
					System.Globalization.CultureInfo.InvariantCulture, 
					FlowDirection.LeftToRight,
					new Typeface("Verdana"), 
					0.03, Brushes.Black, 1.0);
			Gr.Children.Add(
				new GeometryDrawing(
					Brushes.Black, new Pen(Brushes.Black, 0.01), 
					Timestamp.BuildGeometry(timestampPosition)));

			return Gr;
		}

		/// <summary>
		/// Нарисовать полигон.
		/// </summary>
		/// <param name="polygon">Координаты вершин полигона</param>
		/// <param name="brush">Кисть для рисования</param>
		/// <returns>Объект отрисовки WPF (<c>DrawingVisual</c>)</returns>
		public static DrawingVisual DrawPolygon(List<Point> polygon, Brush brush)
		{
			var Gr = new DrawingVisual();
			using (DrawingContext Vdc = Gr.RenderOpen())
			{
				var pen = new Pen(brush, 2);

				Vdc.DrawLine(pen, polygon[polygon.Count - 1], polygon[0]);
				for (int i = 1; i < polygon.Count; i++)
					Vdc.DrawLine(pen, polygon[i - 1], polygon[i]);
			}
			return Gr;
		}
	}
}
