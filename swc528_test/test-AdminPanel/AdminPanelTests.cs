/// AdminPanelTests
/// 25.05.21
/// 26.05.21
/// ���������� �.

using Microsoft.VisualStudio.TestTools.UnitTesting;
using AdminPanel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace AdminPanel.Tests
{
    /// <summary>
    /// �������� ����� ��� ������������ ������ ��������������.
    /// </summary>
    [TestClass]
    public class AdminPanelTests
    {
        /// <summary>
		/// ������� ��� ������������ �������������� ��������� ������ ��������������.
		/// </summary>
        [TestMethod]
        public void TestPlatform_1()
        {
            // ������������ �������������� ���������
            try
            {
                ResearchModel.Logger.InitLogger();
                bool testPlatform = true;
                Assert.IsTrue(testPlatform);
            }
            catch (Exception ex)
            {
                Console.WriteLine("");
                ResearchModel.Logger.WriteLine("Main Exception Type=" + ex.GetType());
                ResearchModel.Logger.WriteLine("Main Exception Message => " + ex.Message);
                ResearchModel.Logger.WriteLine("Main Exception Stack => " + ex.StackTrace);
                int cnt = 1;
                Exception exx = ex.InnerException;
                while (exx != null)
                {
                    ResearchModel.Logger.WriteLine("Inner Exception[" + cnt + "] Type=" + exx.GetType());
                    ResearchModel.Logger.WriteLine("Inner Exception[" + cnt + "] Message => " + exx.Message);
                    ResearchModel.Logger.WriteLine("Inner Exception[" + cnt + "] Stack => " + exx.StackTrace);
                    cnt++;
                    exx = exx.InnerException;
                }
            }
        }
    }
}
