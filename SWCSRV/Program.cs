﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Threading;
using NetworkAdapters;
using Google.Protobuf.COOM;

namespace ServerService
{
	/// <summary>
	/// Основной класс работы с сервисом
	/// </summary>
	/// <list type="bullet">
	/// <item>
	/// <term>configFilePath</term>
	/// <description>Путь к файлу конфигураций программы</description>
	/// </item>
	/// <item>
	/// <term>timeout</term>
	/// <description>Время ожидания ответа от сервера</description>
	/// </item>
	/// <item>
	/// <term>server</term>
	/// <description>Используемый сервер для общения с клиентом</description>
	/// </item>
	/// </list>
	/// 
	class Program
	{
		static readonly string configFilePath = "";
		static readonly int timeout = 500;

		static NetMQServer server;

		/// <summary>
		/// Основная функция запуска работы сервера
		/// </summary>
		/// <param name="args">Стандартные аргументы запуска</param>
		static void Main(string[] args)
		{
			Console.WriteLine("Initializing COOM Monitoring Service");
			Dictionary<string, object> properties = null;
			server = null;
			try {
				properties = LoadServiceProperties();
				server = new NetMQServer(
					properties["Image source address"] as string,
					properties["Publication address"] as string,
					properties["Response address"] as string);
				server.ReadyRead += ServiceRecieveData;
				server.ReadyRespond += ServiceRespond;
			}
			catch (Exception ex)
			{
				Console.WriteLine("Initialization failed; caught exception:");
				Console.WriteLine(ex.Message);
				return;
			}
			Console.WriteLine("Initialization has been successful.");

			server.Start();
			Console.WriteLine("Monitoring...");
			while (true) {
				string command = Console.ReadLine();
				if (command.Contains("stop")) break;
			}
		}

		/// <summary>
		/// Функция обработки результатов распознавания
		/// </summary>
		/// <param name="message">Сообщение от сервера</param>
		static object ServiceRecieveData(object message)
		{
			var initialState = new ImageProcessorState {
				Image1 = null,
				Image2 = null
			};

			var tokenSource = new CancellationTokenSource();
			var imageProcessor = new ImageProcessor(initialState, tokenSource);

			if (Thread.CurrentThread.ThreadState == ThreadState.AbortRequested)
			{
				tokenSource.Cancel();
				return null;
			}

			imageProcessor.Wait(timeout);
			if (imageProcessor.IsCompleted)
				return imageProcessor.Result;
			else
			{
				tokenSource.Cancel();
				return null;
			}
		}

		/// <summary>
		/// Функция обработки ответов с сервера
		/// </summary>
		/// <param name="message">Сообщение от сервера</param>
		static object ServiceRespond(object message)
		{
			if (message is ConfigurationState)
			{
				return "Reconfiguration is not implemented";
			}
			else if (message is ImageProcessorState)
			{
				var state = message as ImageProcessorState;
				if (state.Image1 == null && state.Image2 == null)
				{
					return "Control is now " + (server.ToggleControl() ? "ENABLED" : "DISABLED");
				}
				else
				{
					return "Event request can't be satisfied, because function is not implemented";
				}
			}
			else throw new ArgumentException();
		}

		/// <summary>
		/// Функция загрузки настроек работы сервиса
		/// </summary>
        static Dictionary<string, object> LoadServiceProperties()
		{
			var result = new Dictionary<string, object>();
			var configFile = new StreamReader(configFilePath);
			while (!configFile.EndOfStream)
			{
				string[] property = configFile.ReadLine().Split('\t');
					
				if ((property[0] == "Image source address") 
					|| (property[0] == "Publication address") 
					|| (property[0] == "Response address"))
				{
					result.Add(property[0], property[1]);
				}
				else if (property[0] == "Minimal detection size")
				{
					result.Add(property[0], double.Parse(property[1]));
				}
				else if ((property[0] == "Inclusion area") || (property[0] == "Exclusion area"))
				{
					var polygon = new List<Point>();
					for (int i = 1; i < property.Length; i++)
					{
						string[] point = property[i].Split(';');
						int x = int.Parse(point[0]);
						int y = int.Parse(point[1]);
						polygon.Add(new Point(x, y));
					}
					result.Add(property[0], polygon);
				}
			}
			configFile.Close();
			return result;
		}
	}
}
