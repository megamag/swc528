﻿using System.Windows;
using System.Windows.Controls;
using NetworkAdapters;

namespace OperatorPanel
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private NetMQClient client;

		public MainWindow()
		{
			InitializeComponent();
			client = new NetMQClient("", "");
			client.ReadyRead += ClientRecieveData;
			client.Start();
		}

		private object ClientRecieveData(object message)
		{
			LB_EventFeed.Items.Add(message as string);
			return null;
		}

		/// <summary>
		/// Обработчик нажатия на кнопку запроса
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Bn_RequestEvent_Click(object sender, RoutedEventArgs e)
		{
			var selectedEvent = LB_EventFeed.SelectedItem as ListBoxItem;
			if (selectedEvent != null)
			{
				var eventData = client.Request(selectedEvent.Content as string) as ImageProcessorState;
			}
			else System.Media.SystemSounds.Beep.Play();
		}

		/// <summary>
		/// Обработчик нажатия на кнопку режима контроляЫ
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Bn_ControlMode_Click(object sender, RoutedEventArgs e)
		{
			client.Request("Toggle control");
		}
	}
}
