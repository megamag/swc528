﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Media3D;
using NetworkAdapters;

namespace AdminPanel
{
	/// <summary>
	/// Логика взаимодействия для MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		private NetMQClient client;

		/// <summary>
		/// Метод для отрисовки выделенной области по заданным координатам
		/// </summary>
		public void DrawPoligon()
		{
			if (Polygon == null) return;
			List<Point3D> poly = new List<Point3D>();
			string[] lines = Includes.Text.Split('\r');
			foreach (var line in lines)
			{
				string[] coords = line.Split(',');
				if (coords.Length < 2) continue;
				double x, y;
				if (double.TryParse(coords[0], out x) && double.TryParse(coords[1], out y))
					poly.Add(new Point3D(x, y, 0));
			}
			Polygon.Positions = new Point3DCollection(poly);
		}

		public MainWindow()
		{
			InitializeComponent();
			client = new NetMQClient("", "");
			var config = new ConfigurationState(CameraIp.Text.Split('.'), 0.0f, Includes.Text.Split('\r'), Excludes.Text.Split('\r'));
			client.Request(config.GetMessage());

		}

		/// <summary>
		/// Обработчик изменения текстового поля
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		private void Includes_TextChanged(object sender, TextChangedEventArgs e)
		{
			DrawPoligon();
		}
	}
}
