﻿using System.Collections.Generic;
using System.Threading;
using NetMQ;
using NetMQ.Sockets;
using Google.Protobuf.COOM;

namespace NetworkAdapters
{
    /// <summary>
	/// Класс-обёртка для сокетов ответов, подписок и публикации
	/// </summary>
	/// <list type="bullet">
	/// <item>
	/// <term>ReadyRead</term>
	/// <description>Ивент для запуска обработки принятых сообщений типа публикиция или запрос</description>
	/// </item>
    /// <item>
	/// <term>ReadyRespond</term>
	/// <description>Ивент для запуска обработки принятых сообщений типа запрос</description>
	/// </item>
	/// <item>
	/// <term>subscribeSocket</term>
	/// <description>Сокет подписки в системе подписчик-публикатор</description>
	/// </item>
    /// <item>
	/// <term>PublisherSocket</term>
	/// <description>Сокет публикации в системе подписчик-публикатор</description>
	/// </item>
    /// <item>
	/// <term>ResponseSocket</term>
	/// <description>Сокет ответа в системе запрос-ответ</description>
	/// </item>
	/// <item>
	/// <term>subscriberThread</term>
	/// <description>Поток обработки сообщений сокета подписчика</description>
	/// </item>
    /// <item>
	/// <term>respondingThread</term>
	/// <description>Поток обработки сообщений сокета ответа</description>
	/// </item>
	/// </list>
	public class NetMQServer
	{
		public event Messaging ReadyRead;
		public event Responding ReadyRespond;

		private SubscriberSocket subscribeSocket;
		private PublisherSocket publishSocket;
		private ResponseSocket responseSocket;

		private Thread subscriberThread;
		private Thread respondingThread;
        /// <summary>
        /// Метод отправки публикации.
        /// </summary>
        /// <param name="data">Сообщение публикации</param>
        public void Publish(object data)
		{
			if ((data as ImageProcessorState).IsDetected)
			{
				var imageEvent = (data as ImageProcessorState).GetMessage();
				var bytes = new byte[imageEvent.CalculateSize()];
				imageEvent.WriteTo(new Google.Protobuf.CodedOutputStream(bytes));
				publishSocket.SendMultipartBytes(bytes);
			}
		}
        /// <summary>
        /// Метод запуска/преостановки потока обработки сообщений подписки.
        /// </summary>
        /// <returns>
        /// Возвращает false в случае успешной работы.
        /// </returns>
        /// <exception cref="ThreadStateException">Метод выбрасывает исключение в случае вызова до запуска потока обработки сообщений подписки.</exception>
        public bool ToggleControl()
		{
			if (subscriberThread.ThreadState == ThreadState.Running) {
				subscriberThread.Suspend();
				return false;
			}
			else if (subscriberThread.ThreadState == ThreadState.Suspended) {
				subscriberThread.Resume();
				return false;
			}
			else throw new ThreadStateException();
		}

        /// <summary>
        /// Метод запуска потока обработки сообщений подписчика (<c>subscriberThread</c>) и потока обработки сообщений ответа (<c>respondingThread</c>).
        /// </summary>
        public void Start()
		{
			if (subscriberThread == null || !subscriberThread.IsAlive)
			{
				subscribeSocket.Subscribe("");

				subscriberThread = new Thread(() => {
					while (true)
					{
						var data = subscribeSocket.ReceiveMultipartBytes();
						var message = new List<byte>();
						foreach (var part in data)
							message.AddRange(part);
						var imageEvent = ImageEvent.Parser.ParseFrom(message.ToArray());
						ReadyRead?.Invoke(new ImageProcessorState(imageEvent));
					}
				});
				subscriberThread.Start();
			}

			if (respondingThread == null || !respondingThread.IsAlive)
			{
				respondingThread = new Thread(() =>
				{
					while (true)
					{
						var data = responseSocket.ReceiveMultipartBytes();
						var message = new List<byte>();
						foreach (var part in data)
							message.AddRange(part);
						var imageEvent = ImageEvent.Parser.ParseFrom(message.ToArray());
						var result = ReadyRespond?.Invoke(new ImageProcessorState(imageEvent)) as ImageProcessorState;
						data = null;
						message = null;

						var response = result.GetMessage();
						var bytes = new byte[response.CalculateSize()];
						response.WriteTo(new Google.Protobuf.CodedOutputStream(bytes));
						responseSocket.SendMultipartBytes(bytes);
					}
				});
				respondingThread.Start();
			}
			
		}

        /// <summary>
        /// Конструктор обёртки на основе адресов для сокетов.
        /// </summary>
        /// <param name="subscribeAddress">Адрес сокета подписки</param>
        /// <param name="publishAddress">Адрес сокета публикации</param>
        /// <param name="responseAddress">Адрес сокета ответа</param>
        public NetMQServer(string subscribeAddress, string publishAddress, string responseAddress)
		{
			if (subscribeAddress.Length == 0)
				throw new HostUnreachableException();
			if (publishAddress.Length == 0)
				publishAddress = "@tcp://localhost:" + GlobalVariables.PublisherPort;
			if (responseAddress.Length == 0)
				responseAddress = "@tcp://localhost:" + GlobalVariables.ResponderPort;
			
			subscribeSocket = new SubscriberSocket(subscribeAddress);
			publishSocket = new PublisherSocket(publishAddress);
			responseSocket = new ResponseSocket(responseAddress);
		}

		~NetMQServer()
		{
			subscriberThread.Abort();
			respondingThread.Abort();
		}
	}
}
