﻿using Google.Protobuf;
using Google.Protobuf.COOM;

namespace NetworkAdapters
{
	/// <summary>
	/// Тип делегата <c>Messaging</c> - для обработки сообщений, принятых подписчиком от публикатора.
	/// </summary>
	/// <param name="message">Принятое сообщение.</param>
	/// <returns>Для сервиса: сообщение с результатом, который необходимо опубликовать.</returns>
	public delegate object Messaging(object message);

	/// <summary>
	/// Тип делегата <c>Responding</c> - для обработки сообщений-запросов.
	/// </summary>
	/// <param name="message">Принятое сообщение-запрос.</param>
	/// <returns>Сообщение-ответ.</returns>
	public delegate object Responding(object message);

	/// <summary>
	/// Класс, содержащий глобальные переменные.
	/// </summary>
	static class GlobalVariables
	{
		/// <value>
		/// Сетевой порт, используемый для публикации сообщений публикаторами и их принятия подписчиками.
		/// </value>
		public static string PublisherPort = "8900";

		/// <value>
		/// Сетевой порт, используемый для обмена запросами и ответами.
		/// </value>
		public static string ResponderPort = "9800";

		/// <value>
		/// Список тегов, используемых публикаторами и подписчиками.
		/// </value>
		public static string[] Topics = { "Regular" };
	}

	/// <summary>
	/// Класс-обёртка для сообщений типа <c>ImageEvent</c>.
	/// </summary>
	/// <list type="bullet">
	/// <item>
	/// <term>Image1, Image2</term>
	/// <description>Изображения для сравнения</description>
	/// </item>
	/// <item>
	/// <term>IsDetected</term>
	/// <description>Индикатор обнаружения отличий</description>
	/// </item>
	/// <item>
	/// <term>DifferentPixelsCount</term>
	/// <description>Количество отличающихся пикселей</description>
	/// </item>
	/// <item>
	/// <term>DetectedRectangle</term>
	/// <description>Координаты углов прямоугольника, описанного вокруг отличающейся области</description>
	/// </item>
	/// <item>
	/// <term>Time</term>
	/// <description>Метка времени кадра</description>
	/// </item>
	/// </list>
	public class ImageProcessorState
	{
		public byte[] Image1;
		public byte[] Image2;

		public bool IsDetected;
		public uint DifferentPixelsCount;
		public byte[] DetectedRectangle;
		public ulong Time;

		/// <summary>
		/// Стандартный конструктор.
		/// </summary>
		public ImageProcessorState() { }

		/// <summary>
		/// Конструктор обёртки на основе полученного сообщения.
		/// </summary>
		/// <param name="imageEvent">Сообщение типа <c>ImageEvent</c></param>
		public ImageProcessorState(ImageEvent imageEvent)
		{
			Image1 = imageEvent.Image1.ToByteArray();
			Image2 = imageEvent.Image2.ToByteArray();
			IsDetected = imageEvent.IsDetected;
			DifferentPixelsCount = imageEvent.DifferentPixelsCount;
			DetectedRectangle = imageEvent.Rectangle.ToByteArray();
			Time = imageEvent.Time;
		}

		/// <summary>
		/// Генератор сообщения на основе данных, заложенных в объект класса-обёртки.
		/// </summary>
		/// <returns>Сообщение типа <c>ImageEvent</c></returns>
		public ImageEvent GetMessage()
		{
			return new ImageEvent
			{
				Image1 = ByteString.CopyFrom(Image1 as byte[]),
				Image2 = ByteString.CopyFrom(Image2 as byte[]),
				IsDetected = IsDetected,
				DifferentPixelsCount = DifferentPixelsCount,
				Rectangle = ByteString.CopyFrom(DetectedRectangle as byte[]),
				Time = Time
			};
		}
	}

	/// <summary>
	/// Класс-обёртка для сообщений типа <c>Configuration</c>.
	/// </summary>
	/// <list type="bullet">
	/// <item>
	/// <term>CameraIp</term>
	/// <description>IP-адрес источника изображений (камеры наблюдения)</description>
	/// </item>
	/// <item>
	/// <term>MinAngleSize</term>
	/// <description>Минимальный угловой размер обнаруживаемого объекта</description>
	/// </item>
	/// <item>
	/// <term>PointsIncludes</term>
	/// <description>Вершины полигона, описывающего включающую зону</description>
	/// </item>
	/// <item>
	/// <term>PointsExcludes</term>
	/// <description>Вершины полигона, описывающего исключающую зону</description>
	/// </item>
	/// </list>
	public class ConfigurationState
	{
		public byte[] CameraIp;
		public float MinAngleSize;
		public byte[] PointsIncludes;
		public byte[] PointsExcludes;

		public ConfigurationState() { }

		/// <summary>
		/// Конструктор обёртки на основе данных из окна панели администратора (<c>AdminPanel</c>).
		/// </summary>
		public ConfigurationState(string[] cameraIp, float minAngleSize, string[] linesIncludes, string[] linesExcludes)
		{
			CameraIp = new byte[4];
			for (int k = 0; k < 4; k++)
				CameraIp[k] = byte.Parse(cameraIp[k]);

			MinAngleSize = minAngleSize;

			PointsIncludes = new byte[4 * linesIncludes.Length];
			for (int k = 0; k < linesIncludes.Length; k++)
			{
				string[] coords = linesIncludes[k].Split(',');
				if (coords.Length != 2) continue;
				double x, y;
				if (double.TryParse(coords[0], out x) && double.TryParse(coords[1], out y))
				{
					PointsIncludes[k + 0] = (byte)(x * 1920 / 256);
					PointsIncludes[k + 1] = (byte)(x * 1920 % 256);
					PointsIncludes[k + 2] = (byte)(y * 1080 / 256);
					PointsIncludes[k + 3] = (byte)(y * 1080 % 256);
				}
			}

			PointsExcludes = new byte[4 * linesExcludes.Length];
			for (int k = 0; k < linesExcludes.Length; k++)
			{
				string[] coords = linesExcludes[k].Split(',');
				if (coords.Length != 2) continue;
				double x, y;
				if (double.TryParse(coords[0], out x) && double.TryParse(coords[1], out y))
				{
					PointsExcludes[k + 0] = (byte)(x * 1920 / 256);
					PointsExcludes[k + 1] = (byte)(x * 1920 % 256);
					PointsExcludes[k + 2] = (byte)(y * 1080 / 256);
					PointsExcludes[k + 3] = (byte)(y * 1080 % 256);
				}
			}
		}

		/// <summary>
		/// Конструктор обёртки на основе полученного сообщения.
		/// </summary>
		/// <param name="imageEvent">Сообщение типа <c>Configuration</c></param>
		public ConfigurationState(Configuration message)
		{
			CameraIp = message.CameraIp.ToByteArray();
			MinAngleSize = message.MinAngleSize;
			PointsIncludes = message.PointsIncludes.ToByteArray();
			PointsExcludes = message.PointsExcludes.ToByteArray();
		}

		/// <summary>
		/// Генератор сообщения на основе данных, заложенных в объект класса-обёртки.
		/// </summary>
		/// <returns>Сообщение типа <c>Configuration</c></returns>
		public Configuration GetMessage()
		{
			return new Configuration
			{
				CameraIp = ByteString.CopyFrom(CameraIp),
				MinAngleSize = MinAngleSize,
				PointsIncludes = ByteString.CopyFrom(PointsIncludes),
				PointsExcludes = ByteString.CopyFrom(PointsExcludes)
			};
		}
	}
}
